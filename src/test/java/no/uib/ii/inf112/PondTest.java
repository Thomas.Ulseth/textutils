package no.uib.ii.inf112;

import no.uib.ii.inf112.impl.MySetImpl;
import no.uib.ii.inf112.pond.Pond;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PondTest {
    Pond testPond;

    @BeforeEach
    void createPond() throws Exception {
        testPond = new Pond(10,10);
    }

    @Test
    void firstTest() {
        testPond.step();
        System.out.println(testPond.objs);
    }

}
